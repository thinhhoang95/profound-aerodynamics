% WINGMAN 0.1
% A very simple and intuitive NACA lift and drag solver
%  By Hoang Dinh Thinh (thinh@neuralmetrics.net)

clear all;
clc;

MW=2;
PW=4;
XW=12; % NACA 2412

zero_lift_angle = zero_lift_angle(MW,PW,XW);
alpha = 8; % 8 degree

M=0.7; % Mach number: <0.3 incompr, >0.3 compr

% Rectangular wing
AR=8;
TR=0.6;
cd0=0.009; % form drag
KL=kl(TR,AR);
KD=kd(TR,AR);

% i=0;
% for alpha=-1:0.01:8 
%     i=i+1;
clm = cl3d(M, 30, KL, AR, zero_lift_angle, alpha);
cdm = cd3d(M, clm, cd0, KD, AR);
% end

% plot(-1:0.01:8,clm,'r')
% hold on
% plot(-1:0.01:8,cdm,'g')
% hold off